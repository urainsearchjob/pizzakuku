# VPC
resource "aws_vpc" "terra_vpc" {
  cidr_block       = var.vpc_cidr
  tags = {
    Name = "tf-KUKU-VPC"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "terra_igw" {
  vpc_id = aws_vpc.terra_vpc.id
  tags = {
    Name = "tf-igw"
  }
}

# Subnets : public
resource "aws_subnet" "public" {
  count = length(var.subnets_cidr)
  vpc_id = aws_vpc.terra_vpc.id
  cidr_block = element(var.subnets_cidr,count.index)
  availability_zone = element(var.azs,count.index)
  map_public_ip_on_launch = true
  tags = {
    Name = "tf-subnet-${count.index+1}"
  }
}

# Route table: attach Internet Gateway 
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.terra_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.terra_igw.id
  }
  tags = {
    Name = "publicRouteTable"
  }
}

# Route table association with public subnets
resource "aws_route_table_association" "a" {
  count = length(var.subnets_cidr)
  subnet_id      = element(aws_subnet.public.*.id,count.index)
  route_table_id = aws_route_table.public_rt.id
}

#### ------------------------------   18.06 21:48

#resource "aws_network_interface" "a" {
#  subnet_id = element(aws_subnet.public.*.id)
#  private_ips = ["10.20.1.100"]

#  tags = {
#    Name = "primary_network_interface"
#  }
#}

resource "aws_instance" "a" {
  ami           = "ami-0e4f1e7fc25452026" 
  instance_type = "t2.micro"
  #network_interface {
  #  network_interface_id = aws_network_interface.a.id
  #  device_index         = 0
  #}
  tags = {
   Name = "LOL_U_DID_IT"
 }
}
